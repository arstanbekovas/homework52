import React, { Component } from 'react';
import './App.css';
import Number from "./Components/Number";

class App extends Component {
    state = {
        numbers: []
    };

    generateArray = () => {
        let array = [];
        for (let i = 5; i < 37; i++) {
            array.push(i);
        }
        return array;
    };
    generateNumber = () => {
        let getArry = this.generateArray();
        let secondArray=[];

        for ( let i = 0; i < 5; i++) {
            let randomNumber = Math.floor(Math.random() * getArry.length);
            secondArray.push(getArry[randomNumber]);
            getArry.splice(randomNumber,1);
        }
        this.setState({numbers:secondArray.sort((a, b) => a > b)});

    };

    componentDidMount = () => {
        this.generateNumber();
    };


    render() {
        return (
            <div className="App">
                <div><button onClick={this.generateNumber}>New numbers</button></div>

                <Number number1 = {this.state.numbers[0]}
                        number2 = {this.state.numbers[1]}
                        number3 = {this.state.numbers[2]}
                        number4 = {this.state.numbers[3]}
                        number5 = {this.state.numbers[4]} />

            </div>
        );
    }
}

export default App;
