import React from 'react';
import "./Number.css";

const Number = (props) => {
    return (
        <div className = "numbers">
            <p className = "number">{props.number1}</p>
            <p className = "number">{props.number2}</p>
            <p className = "number">{props.number3}</p>
            <p className = "number">{props.number4}</p>
            <p className = "number">{props.number5}</p>

        </div>
    );
};

export  default Number;